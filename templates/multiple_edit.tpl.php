<table class="media6k_multi_edit media6k_table" cellpadding="0" cellspacing="0">
	<thead>
		<th class="textleft" colspan="3">
			Bulk Editing Multiple Assets ([_count_]):
		</th>
	</thead>
	<tbody>
		<tr class="form_row">
			<td colspan="3">
				[_images_]
				<input type="hidden" autocomplete="off" value="[_image-ids_]" name="multi_edit[ids]" class="media6k_tag_slugs_list">
			</td>
		</tr>
		<tr class="form_row">
			<td class="w17">
				<!--<div class="inner_row">
					<label for="multi_title">Title</label>
					<input id="multi_title" type="text" autocomplete="off" value="" placeholder="title here ..." name="multi_edit[post_title]" />
				</div>
				<div class="inner_row">
					<label for="multi_desc">Description</label>
					<textarea id="multi_desc" autocomplete="off" rows="4" name="multi_edit[post_content]"></textarea>
				</div>-->
				<div class="inner_row">
					<label for="multi_date">Date</label>
					<input id="multi_date" type="text" autocomplete="off" value="" name="multi_edit[post_date]" class="media6k_datepicker" />
				</div>
			</td>
			<td class="w20">
				[_other-fields_]				
			</td>
			<td>
				<div class="inner_row">
					<label>Media Tags</label>
					<div class="current_tags_markup">none</div>							
					<input type="hidden" autocomplete="off" value="" name="multi_edit[edited_tags]" class="media6k_tags_edited_bool" />
					<input type="hidden" autocomplete="off" value="" name="multi_edit[tax_input][media_tag]" class="media6k_tag_slugs_list" />
					<div>
						<label><a class="media6k_toggle_tags_list">add tags +</a></label>
					</div>
				</div>
				<div class="inner_row">
					<label>Media Categories</label>
					<div class="current_cats_markup">none</div>							
					<input type="hidden" autocomplete="off" value="" name="multi_edit[edited_cats]" class="media6k_cats_edited_bool" />
					<input type="hidden" autocomplete="off" value="" name="multi_edit[tax_input][media_category]" class="media6k_cat_slugs_list" />
					<div>
						<label><a class="media6k_toggle_cats_list">add categories +</a></label>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th class="textright" colspan="3">
				<input class="button button-large button-primary" type="submit" value="save changes to all selected images" />
			</th>
		</tr>
	</tfoot>
</table>