<?php 
	

// ================================== add extra meta on image upload ... etc ... image

function img6k_add_meta($post_ID, $timeStamp = null) {
	
	// set to today, if the att isn't an image ...
	if(is_null($timeStamp) && !strstr(get_post_mime_type($post_ID), 'image'))
		$timeStamp = date('Y-m-d H:i:s');
	
	$img = new img6k($post_ID);
	if(!!$img->ID) 
		$img->set_ts($timeStamp);
		
	update_post_meta($post_ID, 'exclude', 'narp');	
	update_post_meta($post_ID, 'grid_exclude', 'narp');
		
	return $post_ID;
	
}
add_filter('add_attachment', 'img6k_add_meta', 10, 2);





/**
* add custom field to gallery popup 
*/
function add_image_attachment_fields_to_edit($form_fields, $post) {
	
	// avaliable for purchase ?
	$avail = get_post_meta($post->ID, "available", true);
	$opts = [ // label, value, disabled
			['yarp', 'yarp', true],
			['coming soon', 'coming soon', true],
			['------', '', true],
			['narp', 'narp', true],
			['sold', 'sold', true],
			['sold out', 'sold out', true],
			['temporarily unavailable', 'temporarily unavailable', true]
		];
	$html = "<select id='attachments[{$post->ID}][available]' name='attachments[{$post->ID}][available]'>";
	foreach($opts as $k => $o) {
		$value = $o[1] != '' ? 'value="' . $o[1] . '"' : '';
		$selected = $o[1] == $avail || ($o[1] == 'narp' && $avail == '')  ? 'selected' : '';
		$disabled = !$o[2] ? 'disabled' : '';
		$html .= '<option ' . $value . ' ' . $selected . ' ' . $disabled . '>' . $o[0] . '</option>';
	}
	$html .= '</select>';
	$form_fields["available"] = [
			"label" => __("Available For Purchase?"),
			"input" => "html", // text, textarea, html ...
			"value" => get_post_meta($post->ID, "available", true),
			"html" => $html
		];
		
	// purchase link	
	$form_fields["purchase_link"] = [
			"label" => __("Purchase Link"),
			// "helps" => __("(optional) URL of where to but this."),
			"input" => "text", // text, textarea, html ...
			"value" => get_post_meta($post->ID, "purchase_link", true)
		];
		
	// exclude from arts pages?
	$exclude = get_post_meta($post->ID, "exclude", true);
	$exclude_options = ['narp', 'yarp'];
	$exclude_html = ""; //"<select id='attachments[{$post->ID}][exclude]' name='attachments[{$post->ID}][exclude]'>";
	foreach($exclude_options as $k => $o) {
		$value = 'value="' . $o . '"';
		$checked = $o == $exclude || ($o == 'narp' && $exclude != 'yarp') ? 'checked' : '';
		$exclude_html .= '<span style="padding-right:1em;"><input type="radio"  ' . $value . ' ' . $checked . ' name="attachments[' . $post->ID . '][exclude]" /> ' . $o . ' </span>';
	}
	//$exclude_html .= '</select>';
	$form_fields["exclude"] = [
			"label" => __("Exclude from Arts Pages?"),
			// "helps" => __("(optional) URL of where to but this."),
			"input" => "html", // text, textarea, html ...
			"value" => $exclude,
			"html" => $exclude_html
		];
		
	// exclude from exhibition grid?
	$grid_exclude = get_post_meta($post->ID, "grid_exclude", true);
	$grid_exclude_options = ['narp', 'yarp'];
	$grid_exclude_html = ""; //"<select id='attachments[{$post->ID}][exclude]' name='attachments[{$post->ID}][exclude]'>";
	foreach($grid_exclude_options as $gk => $go) {
		$grid_value = 'value="' . $go . '"';
		$grid_checked = $go == $grid_exclude || ($go == 'narp' && $grid_exclude != 'yarp') ? 'checked' : '';
		$grid_exclude_html .= '<span style="padding-right:1em;"><input type="radio"  ' . $grid_value . ' ' . $grid_checked . ' name="attachments[' . $post->ID . '][grid_exclude]" /> ' . $go . ' </span>';
	}
	//$grid_exclude_html .= '</select>';
	$form_fields["grid_exclude"] = [
			"label" => __("Exclude from Exhibition Grid?"),
			// "helps" => __("(optional) URL of where to but this."),
			"input" => "html", // text, textarea, html ...
			"value" => $grid_exclude,
			"html" => $grid_exclude_html
		];
		
	// use as post thumbnail?
	$hasParent = is_int( $post->post_parent );
	if($hasParent) {
		
		$parent = get_post($post->post_parent);
		$parentTitle = $parent->post_title;
		$label = 'Use as the Thumbnail for "' . $parentTitle . '" ?';
		
		$is_post_thumbnail = get_post_meta($post->ID, "is_post_thumbnail", true);
		$is_post_thumbnail_options = ['narp', 'yarp'];
		$is_post_thumbnail_html = ""; //"<select id='attachments[{$post->ID}][exclude]' name='attachments[{$post->ID}][exclude]'>";
		foreach($is_post_thumbnail_options as $gk => $go) {
			$grid_value = 'value="' . $go . '"';
			$grid_checked = $go == $is_post_thumbnail || ($go == 'narp' && $is_post_thumbnail != 'yarp') ? 'checked' : '';
			$is_post_thumbnail_html .= '<span style="padding-right:1em;"><input type="radio"  ' . $grid_value . ' ' . $grid_checked . ' name="attachments[' . $post->ID . '][is_post_thumbnail]" /> ' . $go . ' </span>';
		}
		//$is_post_thumbnail_html .= '</select>';
		$form_fields["is_post_thumbnail"] = [
				"label" => __($label),
				// "helps" => __("(optional) URL of where to but this."),
				"input" => "html", // text, textarea, html ...
				"value" => $is_post_thumbnail,
				"html" => $is_post_thumbnail_html
			];
	} 
	
	//
	//
	//
	return $form_fields;
	
}
add_filter("attachment_fields_to_edit", "add_image_attachment_fields_to_edit", null, 2);

function add_image_attachment_fields_to_save($post, $attachment) {
	// $attachment part of the form $_POST ($_POST[attachments][postID])
	// $post['post_type'] == 'attachment'
	$ID = $post['ID'];
	if( isset($attachment['purchase_link']) ){
		update_post_meta($ID, 'purchase_link', $attachment['purchase_link']);
	}
	if( isset($attachment['available']) ){
		update_post_meta($ID, 'available', $attachment['available']);
	}
	if( isset($attachment['exclude']) ){
		update_post_meta($ID, 'exclude', $attachment['exclude']);
	}
	if( isset($attachment['grid_exclude']) ){
		update_post_meta($ID, 'grid_exclude', $attachment['grid_exclude']);
	}
	if( isset($attachment['is_post_thumbnail']) ){
		// we have to unset the others is this is set to yarp
		$is_post_thumbnail = $attachment['is_post_thumbnail'];
		if($is_post_thumbnail == 'yarp') {
			$attPost = get_post($ID);
			$parent = get_post($attPost->post_parent);
			$otherAtts = get_posts('numberposts=-1&post_type=attachment&post_mime_type=image&post_status=null&post_parent=' . $parent->ID . '&exclude=' . $post['ID']);
			if(!empty($otherAtts)) {
				foreach($otherAtts as $att) {
					update_post_meta($att->ID, 'is_post_thumbnail', 'narp');
				} 
			}
		}
		update_post_meta($post['ID'], 'is_post_thumbnail', $attachment['is_post_thumbnail']);
	}
	
	//
	// @todo :
	// store the output of some of the im6k things for easier retrieval
	//
	// $img6k = new img6k($ID);
	// update_post_meta($ID, 'img6k_src_markup', $attachment['is_post_thumbnail']);
	
	
	return $post;
}
add_filter("attachment_fields_to_save", "add_image_attachment_fields_to_save", null , 2);





//
//
//

/**
* get img ID from original src
*/
private function get_id_from_url($url) {
	global $wpdb;
	$prefix = $wpdb->prefix;
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM " . $prefix . "posts" . " WHERE guid='%s';", $url ));
	return $attachment[0];
} //


/**
* get exif data ...
*/
public function get_exif() {
	
	return exif_read_data($this->get_file_path(), 0, true);
	
} //

/**
* get exif timestamp
*/
public function get_exif_ts() {
	
	$exif = $this->get_exif();
	return isset($exif['EXIF']) && isset($exif['EXIF']['DateTimeOriginal']) ? $exif['EXIF']['DateTimeOriginal'] : date('Y:m:d H:i:s');
		
} //

/**
* set ts in meta
*/
public function set_ts($TS = null) {
	
	$TS = is_null($TS) ? date('Y-m-d H:i:s', strtotime($this->get_exif_ts())) : $TS;
	if($TS)
		add_post_meta( $this->ID, 'image-timestamp', $TS, true ) || update_post_meta( $this->ID, 'image-timestamp', $TS );
	
} //

/**
*
*/
public function get_ts() {
	return get_post_meta( $this->ID, 'image-timestamp', true);
} //




//
// remove default caption jams
//
add_filter( 'disable_captions', create_function('$a', 'return true;') );

function img6k_shortcode_helper() {
	add_action( 'print_media_templates', 'img6k_shortcode_template' );
        add_action( 'admin_print_footer_scripts', 'img6k_add_editor_script', 100 );
} // img6k_shortcode_helper()

function img6k_shortcode_template() {
	?>	
	<script type="text/html" id="tmpl-img6k_shortcode">
		<figure class="{{ data.classes }}">
			<img src="{{ data.def_src }}" />
			<# if ( data.caption ) { #>
			<figcaption>
				{{ data.caption }}
			</figcaption>
			<# } #>
		</figure>
	</script>
	<?php
} // img6k_shortcode_template()

function img6k_add_editor_script() {
	echo '<script type="text/javascript">';
		echo file_get_contents( plugins_url('js/img6k_edit_mode.js', __FILE__) );
	echo '</script>';
} // img6k_add_editor_script()

function img6k_shortcode( $atts, $content = null ) {
	
	$a = shortcode_atts( array(
		'id' => 0,
		'classes' => '',
		'caption' => '',
		'url' => ''
	), $atts );
	
	$img = new img6k($a['id']);
	$args = [
			'post' => $img->post,
			'figClass' => 'figure6k ' . $a['classes'],
			'captionText' => $a['caption']
		];
	if(isset($a['url']) && $a['url'] != '' && !is_null($a['url']))
		$args['linkTo'] = $a['url'];
		
	$figure = new figure6k($args);
	
	return $figure->HTML;
}
add_shortcode( 'img6k', 'img6k_shortcode' );


function img6k_insert_image($html, $id, $caption, $title, $align, $url, $size, $alt) {
	
	$img = new img6k($id);
	
	$atts = [
		'id' => $id,
		'classes' => 'size-' . $size . ' align' . $align,
		'caption' => $caption,
		'align' => $align,
		'size' => $size,
		'url' => $url,
		'def_src' => $img->srcs->medium
		];
	$attsString = '';
	foreach($atts as $k => $v) { $attsString .= $k . '="' . $v . '" '; }
	
	return  "\n\n" . '[img6k ' . $attsString . ' /]' . "\n\n";
	
}
add_filter( 'image_send_to_editor', 'img6k_insert_image', 10, 9 );

function img6k_replace_wp_images($content) {
	
	if(preg_match_all("/<img .*?wp-image-(\d+).*? \/>/", $content, $matches)) {
		foreach($matches[0] as $k => $img) {
						
			$addClasses = '';
			$replace = null;
			if(preg_match('/class="(.*?)"/', $img, $classMatches)) {
				$addClasses = $classMatches[1];
			}

			
			$img_id = $matches[1][$k];
			
			$getAttachment = get_posts('numberposts=1&post_type=attachment&post_status=null&orderby=sort_column&order=ASC&include=' . $img_id);	
			
			if($getAttachment) {	
				
				$figure = new figure6k([
					'post' => $getAttachment[0],
					'figClass' => 'figure6k ' . $addClasses
				]);
				$replace =  $figure->HTML . '<script type="text">' . $figure->HTML . '</script>';
			
			}
			
			if(!is_null($replace)) {
				$content = preg_replace($img, '[JUNK]', $content);
				$content = preg_replace("/<\[JUNK\]>/", $replace, $content);
			}
		}
	}
	
	return $content;
}
add_filter('the_content', 'img6k_replace_wp_images');


function img6k_add_editor_styles() {
   // add_editor_style( 'plugins/image_jammer_6k/css/img6k_edit_mode.css' );
   // wp_enqueue_script('img6k_edit_mode', plugins_url('js/img6k_edit_mode.js', __FILE__));
   //add_editor_style('style.min.css');
}
add_action( 'admin_init', 'img6k_add_editor_styles' );