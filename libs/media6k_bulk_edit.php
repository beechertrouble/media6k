<?php

//
// bulk edit page
//


function media6k_page() {
	
	//
	// the save actions ...
	//
	if(wp_verify_nonce($_POST['media6k_bulk_noncename'], 'media6k_bulk')) {
		
		$changed = [];
		$saveFields = [];
		
		if(isset($_POST['changed'])) {
			
			$changed = explode(",", $_POST['changed']);
			$saveFields = $_POST;
			
		}
		
		if(isset($_POST['multi_edit'])) {
			
			$changed = explode(",", $_POST['multi_edit']['ids']);
			unset($_POST['multi_edit']['ids']);
			
			$tagsChanged = $_POST['multi_edit']['edited_tags'] == 'yarp';
			unset($_POST['multi_edit']['edited_tags']);
			
			if(!$tagsChanged)
				unset($_POST['multi_edit']['tax_input']['media_tag']);
				
			$catsChanged = $_POST['multi_edit']['edited_cats'] == 'yarp';
			unset($_POST['multi_edit']['edited_cats']);
			
			if(!$catsChanged)
				unset($_POST['multi_edit']['tax_input']['media_category']);
				
			$saveFields = [];
			foreach($changed as $ID) {

				foreach($_POST['multi_edit'] as $k => $v) {

					if(!array_key_exists($k, $saveFields))
						$saveFields[$k] = [];
					
					$saveFields[$k][$ID] = $v;

				}
			}
									
		}
		
		if(!empty($changed)) {
			foreach($changed as $id) {
				
				$update = [ 'ID' => $id ];
				
				foreach($saveFields as $k => $v) {
					
					if(!is_array($v) || !array_key_exists($id, $v)) continue;
					
					switch($k) {
						
						// attachments = post_meta
						case('attachments'):
							foreach($v[$id] as $mk => $mv) {
								update_post_meta($id, $mk, $mv);
								// echo '<br />update_post_meta<pre>'; print_r([$id, $mk, $mv]); echo '</pre><br />';
							}
							break;
							
						// tax_input = tax_input['media_tag']
						case('tax_input'):
							$thisOne = $v[$id];
							if(isset($thisOne['media_tag'])) {
								$update['tax_input']['media_tag'] = $thisOne['media_tag'];
								// $update['newtag'] = 'media_tag';
							}
							if(isset($thisOne['media_category'])) {
								// $update['newmedia_category'] = 'New Media Category';
								// $update['newmedia_category_parent'] = -1;
								$update['tax_input']['media_category'] = explode(",", $thisOne['media_category']);
								
							}
							break;
						
						// no empty dates yall	
						case('post_date'):
							$cleanedDate = preg_replace("/[^0-9\:\-]/", "", $v[$id]); 
							if(!$cleanedDate == '') {
								$update[$k] = $v[$id];
							} 
							break;
							
						default:
							$update[$k] = $v[$id];
							break;
						
					}
					// echo '<br />' . $k . '<pre>'; print_r($v[$id]); echo '</pre><br />';
					
				}
				$updated = wp_update_post($update); 
				
			}
		}
		
				
	} // wp_verify_nonce
	
	
	//
	// style
	//
	echo '<style type="text/css">' 
		. file_get_contents( media6k_plugin::asset('css/gallery6k_edit_mode.css') ) 
		. file_get_contents( media6k_plugin::asset('css/jquery_timepicker.css') )
	. '</style>';
	
	//
	// some templates
	//
	$img_tags = get_terms('media_tag', [ 'hide_empty' => 0 ]);
	$img_cats = get_terms('media_category', [ 'hide_empty' => 0 ]);
	

	echo '<script id="media6k_tags_list_template" type="text/html">';
		echo '<div class="media6k_tags_lister">';
			$tags_list = [];
			$tags_data = [];
			if(!empty($img_tags)) {
				foreach($img_tags as $k => $t) {
					$tags_list[] = '<a class="media6k_add media6k_add_tag" href="#" data-slug="' . $t->slug . '">' . $t->name . '</a>';
					$tags_data[$t->slug] = $t->name;
				}
			}
			echo implode(", ", $tags_list);
		echo '</div>';
	echo '</script>';
	echo '<script type="text/javascript">';
		echo '_media6k_tags_data = ' . json_encode($tags_data) . ';';
	echo '</script>';
	
	echo '<script id="media6k_cats_list_template" type="text/html">';
		echo '<div class="media6k_cats_lister">';
			$cats_list = [];
			$cats_data = [];
			if(!empty($img_cats)) {
				foreach($img_cats as $k => $c) {
					$cats_list[] = '<a class="media6k_add media6k_add_cat" href="#" data-slug="' . $c->term_id . '">' . $c->name . '</a>';
					$cats_data[$c->term_id] = $c->name;
				}
			}
			echo implode(", ", $cats_list);
		echo '</div>';
	echo '</script>';
	echo '<script type="text/javascript">';
		echo '_media6k_cats_data = ' . json_encode($cats_data) . ';';
	echo '</script>';
	
	echo '<script id="media6k_tag_markup_template" type="text/html">';
		echo file_get_contents( media6k_plugin::asset('templates/single_tag_remove.tpl.php') );
	echo '</script>';
	
	echo '<script id="media6k_cat_markup_template" type="text/html">';
		echo file_get_contents( media6k_plugin::asset('templates/single_cat_remove.tpl.php') );
	echo '</script>';
	
	echo '<script id="media6k_other_fields_template" type="text/html">';
		$otherFieldsTemplate = []; //add_image_attachment_fields_to_edit([], 0);
		foreach($otherFieldsTemplate as $ok => $ov) {
		
			$id = 'multi_' .$ok;
			if($ov['input'] == 'html') {
				$html = preg_replace("/attachments\[\]/", "multi_edit[attachments]", $ov['html']); 
			} else {
				$html = '<input id="' . $id . '" name="multi_edit[attachments][' . $ok . ']" type="text" value="" autocomplete="off" />';
			}
			
			echo '<div class="inner_row">
					<label for="' . $id . '">' . $ov['label'] . '</label>
					' . $html . '
				</div>';
			
		}
	echo '</script>';

	
	//
	// the wrap
	//
	echo '<div id="media6k_bulk_edit_page" class="wrap">
			<h2>media6k Bulk Edit</h2>
			<p>
				click here to : <a href="/wp-admin/edit-tags.php?taxonomy=media_tag&post_type=attachment">manage available image tags</a><br />
				click here to : <a href="/wp-admin/edit-tags.php?taxonomy=media_category&post_type=attachment">manage available image categories</a>
			</p>';
	
	$root = get_bloginfo('wpurl');
	$page_root = $root . '/wp-admin/upload.php?';
	$page_url = $root. "/wp-admin/upload.php?page=media6k_bulk";
	$nonce = wp_create_nonce('media6k_bulk');
	
	$qv = $_GET;
	$ppp = isset($qv['ppp']) ? $qv['ppp'] : -1;
	$p = isset($qv['p']) ? $qv['p'] : 1;

	$args = [
			'posts_per_page' => $ppp,
			'count' => $ppp,
			'orderby' => 'post_date',
			'order' => 'DESC',
			'paged' => $p
		];

	
	if(isset($_GET['mime_filter']) && $_GET['mime_filter'] != 'all') {
		$args['post_mime_type'] = $_GET['mime_filter'];
	}
	
	$allArgs = $args;	
	//unset($allArgs['posts_per_page']);
	//unset($allArgs['count']);
	$all = media6k::get_args($allArgs);

	$args = media6k::get_args($args);
	$results = new WP_Query($args);
	
	$total = $results->found_posts;
	$maxPages = $results->max_num_pages;
	$page = isset($results->query_vars['paged']) ? $results->query_vars['paged'] : 1;	
	$nextPage = $page +1;
	$prevPage = $page -1;
	
	
	echo '<h3>filters : <br />';
		
		$qvt = $qv;
		unset($qvt['p']);
		$mimes = [ 'all', 'image', 'video' ];
		$current = isset($_GET['mime_filter']) ? $_GET['mime_filter'] : 'all';
		$mimeLinks = [];
		foreach($mimes as $m ) {
			
			$my_qv = $qvt;
			$my_qv['mime_filter'] = $m;
			$link = $page_root . http_build_query($my_qv);
			$mimeLinks[] = $current == $m ? $m : '<a href="' . $link . '"> ' . $m  . '</a> ';
			
		}
		echo implode(" | ", $mimeLinks);
	
	echo '</h3>';	
		
	$this_page = $page_root . http_build_query($qv);
	$qvp = $qv;
	$qvp['p'] = $prevPage;
	$prev_link = $page_root . http_build_query($qvp);
	$qvn = $qv;
	$qvn['p'] = $nextPage;
	$next_link = $page_root . http_build_query($qvn);
	
	echo '<script id="media6k_multiple_template" type="text/html">';
		echo '<form id="media6k_multi_bulk_edit" method="POST" action="' . $this_page . '">';
			echo '<input type="hidden" name="media6k_bulk_noncename" value="' . $nonce . '" />';
			echo file_get_contents( media6k_plugin::asset('templates/multiple_edit.tpl.php') );
		echo '</form>';
	echo '</script>';
	
	$pagination = '<div class="media6k_pagination">';
		
		if($prevPage > 0) 
			$pagination .= '<a class="media6k_prev_next media6k_prev" href="' . $prev_link . '">prev</a>';
		
		$pagination .= '<span class="media6k_pagination_onpage">' . $page . '</span>';
		$pagination .= '<span class="media6k_pagination_sep"> / </span>';
		$pagination .= '<span class="media6k_pagination_of">' . $maxPages . '</span>';
		
		if($nextPage <= $maxPages) 
			$pagination .= '<a class="media6k_prev_next media6k_next" href="' . $next_link . '">next</a>';
		
	$pagination .= '</div>';
	
	if(!empty($results->posts)) {		
				
		echo '<form id="media6k_bulk_edit" method="POST" action="' . $this_page . '">';
			
			echo '<input type="hidden" name="media6k_bulk_noncename" value="' . $nonce . '" />';
			echo '<input id="media6k_changed_checker" type="hidden" name="changed" value="" />';
			
		echo '<table class="media6k_table" cellpadding="0" cellspacing="0">
				<thead id="media6k_bulk_edit_fixed_nav">
					<tr>
						<th class="textleft" colspan="3">
							<a id="toggle_all_rows" class="padR" href="#whatever">select all rows</a>
							<input class="multi_edit button button-large button-primary" type="button" value="bulk edit selected images" disabled autocomplete="off" />
						</th>
						<th>' . $pagination . '</th>
					</tr>
				</thead>
				<tbody>';
		
		foreach($results->posts as $k => $p) {
			
			$img = new media6k($p->ID);
			// echo '<br /><pre>'; print_r($img); echo '</pre><br />';
			$otherFields = []; //add_image_attachment_fields_to_edit([], $p);
			
			$parent = $p->post_parent != 0 ? get_post($p->post_parent) : false;
			
			?>
				
				<tr id="row_<?php echo $p->ID; ?>" class="form_row" data-id="<?php echo $p->ID; ?>">
					<td class="w2 textright">
						<input class="media6k_row_select" type="checkbox" name="media6k_row_select[<?php echo $p->ID; ?>]" value="<?php echo $p->ID; ?>" autocomplete="off" />						
					</td>
					<td class="w4">
						<img id="media6k_thumb_<?php echo $p->ID; ?>" class="media6k_thumb" src="<?php echo $img->srcs->thumbnail; ?>"  />
						<div class="inner_row">
							ID : <?php echo $p->ID; ?>
						</div>
					</td>
					<td>
						<div class="inner_row">
							<label for="title_<?php echo $p->ID; ?>">Title</label>
							<input id="title_<?php echo $p->ID; ?>" type="text" name="post_title[<?php echo $p->ID; ?>]" value="<?php echo $p->post_title; ?>" autocomplete="off" />
						</div>
						<div class="inner_row">
							<label for="excerpt_<?php echo $p->ID; ?>">Caption</label>
							<textarea id="excerpt_<?php echo $p->ID; ?>"name="post_excerpt[<?php echo $p->ID; ?>]" rows="2" autocomplete="off" ><?php echo $p->post_excerpt; ?></textarea>
						</div>
						<div class="inner_row">
							<label for="content_<?php echo $p->ID; ?>">Description</label>
							<textarea id="content_<?php echo $p->ID; ?>"name="post_content[<?php echo $p->ID; ?>]" rows="3" autocomplete="off" ><?php echo $p->post_content; ?></textarea>
						</div>
						<div class="inner_row">
							<label for="date_<?php echo $p->ID; ?>">Date</label>
							<input id="date_<?php echo $p->ID; ?>" class="media6k_datepicker" type="text" name="post_date[<?php echo $p->ID; ?>]" value="<?php echo $p->post_date; ?>" autocomplete="off" />
						</div>

					</td>
					<!--
					<td class="w21">
						<?php  
							foreach($otherFields as $k => $v) {
							
								$id = $k . '_' . $p->ID;
								$html = $v['input'] == 'html' ? $v['html'] : '<input id="' . $id . '" name="attachments[' . $p->ID . '][' . $k . ']" type="text" value="' . $v['value'] . '" autocomplete="off" />';
								
								echo '<div class="inner_row">
										<label for="' . $id . '">' . $v['label'] . '</label>
										' . $html . '
									</div>';
								
							}
						?>
					</td>-->
					<td class="w21">
						<div class="inner_row">
							<label>Image Link</label>
							<a href="<?php echo get_permalink($p->ID); ?>"><?php echo get_permalink($p->ID); ?></a>
						</div>
						<div class="inner_row">
							<label>Parent Page</label>
							<?php if(!!$parent) { ?>
								<a href="<?php echo get_permalink($parent->ID); ?>"><?php echo $parent->post_title; ?></a>
							<?php } else { ?>
								none					
							<?php } ?>
						</div>
						<div class="inner_row">
							<label>Image Tags</label>
							<?php  
								$tag_slugs = [];
								if(!is_null($img->tags) && !empty($img->tags)) {
									$tags = [];
									foreach($img->tags as $tk => $tv) {
										$tags[] = '<a class="media6k_remove media6k_remove_tag" href="#" data-slug="' . $tv->slug . '" title="remove this tag">' . $tv->name . '</a>';
										$tag_slugs[] = $tv->slug;
									}
									echo '<div class="current_tags_markup">' . implode("", $tags) . '</div>';
								} else { 
									echo '<div class="current_tags_markup">none</div>';
								}
								$tag_slugs_string = !empty($tag_slugs) ? implode(',', $tag_slugs) : '';
								
							?>
							<input class="media6k_tag_slugs_list" type="hidden" name="tax_input[<?php echo $p->ID; ?>][media_tag]" value="<?php echo $tag_slugs_string; ?>" autocomplete="off" />
							<div>
								<label><a class="media6k_toggle_tags_list">add tags +</a></label>
							</div>
						</div>
						<div class="inner_row">
							<label>Image Cateogries</label>
							<?php  
								$cat_slugs = [];
								if(!is_null($img->categories) && !empty($img->categories)) {
									$cateogries = [];
									foreach($img->categories as $ck => $cv) {
										$cateogries[] = '<a class="media6k_remove media6k_remove_cat" href="#" data-slug="' . $cv->term_id . '" title="remove this category">' . $cv->name . '</a>';
										$cat_slugs[] = $cv->term_id;
									}
									echo '<div class="current_cats_markup">' . implode("", $cateogries) . '</div>';
								} else { 
									echo '<div class="current_cats_markup">none</div>';
								}
								$cat_slugs_string = !empty($cat_slugs) ? implode(',', $cat_slugs) : '';
								
							?>
							<input class="media6k_cat_slugs_list" type="hidden" name="tax_input[<?php echo $p->ID; ?>][media_category]" value="<?php echo $cat_slugs_string; ?>" autocomplete="off" />
							<div>
								<label><a class="media6k_toggle_cats_list">add Cateogries +</a></label>
							</div>
						</div>
						<div class="inner_row">
							<input class="media6k_inline_bulk_submit button button-large button-primary" type="submit" value="save changes to all edited images" disabled  autocomplete="off" /> 
						</div>
					</td>
				</tr>
				
			<?php
		}
		
		echo '<tfoot>
				<tr>
					<th class="textleft" colspan="2"><input class="multi_edit button button-large button-primary" type="button" value="bulk edit selected images" disabled  autocomplete="off" /></th>
					<th></th>
					<th>' . $pagination . '</th>
				</tr>
			</tfoot>';
		
		echo '</tbody></table>';
		
		echo '</form>';
				
		echo '<div class="media6k_modal media6k_hidden">
					<div class="media6k_rel_wrap">
						<div class="textright"><a class="media6k_modal_close">close</a></div>
						<div class="media6k_modal_content"></div>
					</div>
				</div>';		
		
	} // $results->posts
	
	// close the wrap
	echo '</div>';
	
	
	//
	// the scripts
	//
	echo '<script type="text/javascript">';
		echo file_get_contents( media6k_plugin::asset('js/jquery_timepicker.js') );
		echo file_get_contents( media6k_plugin::asset('js/media6k_bulk_edit.js') );
	echo '</script>';
	
} // media6k_page()
