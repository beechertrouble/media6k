<?php
	
/**
*
*/
class figure6k {
	
	public function __construct($args = array()) {
		
		if(!empty($args)) {
		
			if(isset($args['post']))
				$this->post = $args['post'];
				
			if(isset($args['figClass']))
				$this->figClass = $args['figClass'];
				
			if(isset($args['linkTo']))
				$this->linkTo = $args['linkTo']; 
				
			if(isset($args['linkClass']))
				$this->linkClass = $args['linkClass'];
			
			if(isset($args['captionText']))
				$this->captionText = $args['captionText'];
				
			if(isset($args['caption']))
				$this->captionOverride= $args['caption'];
			
			if(isset($args['size']))
				$this->size = $args['size'];
		
			$this->build();
		
		}
		
	} // __construct 
	
	/**
	*
	*/
	public function build() {
		
		if(isset($this->post))
			$this->post = is_array($this->post) ? $this->post[0] : $this->post;
		
		$this->getMedia();
		
		if(property_exists($this, 'captionOverride')) {
			$this->Caption = new stdClass();
			$this->Caption->HTML = $this->captionOverride;
		} else {
			$this->getCaption();
		}
		
		$this->HTML = '<figure id="figure_' . $this->MediaPost->ID. '" class="' . $this->figClass . '">'
			. $this->linkOpen . $this->Media->HTML . $this->linkClose
			. $this->Caption->HTML
			. '</figure>'; 	
												
	} //  build()
	
	/**
	*
	*/
	public function getLink() {
	
		$this->linkOpen = null;
		$this->linkClose = null;
		$this->linkClass = isset($this->linkClass) ? 'class="' . $this->linkClass . '"' : null;
		
		if(isset($this->linkTo)) {
			
			switch ($this->linkTo) {
				
				case 'post':
					$this->linkOpen = '<a ' . $this->linkClass . ' href="' . get_permalink($this->post->ID) . '">';
					$this->linkClose = '</a>';
					break;
					
				case 'parent':
					$this->linkOpen = '<a ' . $this->linkClass . ' href="' . get_permalink($this->parentID) . '">';
					$this->linkClose = '</a>';
					break;
					
				case(null):
					break;
					
				default:
					$this->linkOpen = '<a ' . $this->linkClass . ' href="' . $this->linkTo . '">';
					$this->linkClose = '</a>';
					break;
			
			}
			
		}
	
	} // getLink()
	
	
	/**
	*
	*/
	public function getCaption() {
		
		$this->Caption = new stdclass();
		$this->Caption->HTML = null;
		
		$this->getLink();
						
		switch ($this->linkTo) {
			
			case 'post':
				$page_ID = $this->post->ID;
				break;
				
			case 'parent':
				$page_ID = $this->parentID;
				break;
		
		}
	
		$paypal = function_exists('paypal_form') ? paypal_form($page_ID, null, false, $link = null, $hide = array('item_name', 'item_number'), false) : null;
		
		$this->captionText = $this->captionText != null && $this->captionText != '' ? $this->captionText : $this->img->get_caption();
		
		$hasCaption = ($this->captionText != null && $this->captionText != '') || (!is_null($paypal) && $paypal != '') ? true : false; 
		
		$this->Caption->HTML = $hasCaption ? '<figcaption>' . $this->linkOpen . $this->captionText . $this->linkClose . $paypal . '</figcaption>' : null;
				
			
	} // getCaption()
	
	
	/**
	*
	*/
	public function getMediaPost() {
	
		$this->MediaPost = null;
		
		if($this->post->post_type == 'attachment') {
						
			$this->MediaPost = $this->post;
		
		} else {
		
			$getAttachment = get_posts('numberposts=1&post_type=attachment&post_status=null&orderby=sort_column&order=ASC&post_parent=' . $this->post->ID);	
			
			if($getAttachment) {
			
				$this->MediaPost = $getAttachment[0];
				
			}
		
		}
		
	
	} // getMediaPost()
	
	
	/**
	*
	*/
	public function getMedia() {
		
		$this->getMediaPost();
		
		if(!is_null($this->MediaPost)) {
		
			if(strstr($this->MediaPost->post_mime_type, 'image')) { // image …
				
				$this->img = new img6k($this->MediaPost->ID);	
				
				$this->Media = new stdclass();
				$this->Media->HTML = $this->img->lazy_markup(null, '_picky');
			
			} else { // video ...
			
				echo '<br /><pre>'; print_r('@todo'); echo '</pre><br />';
			
			}
		
		}
	
	} // getMedia()

}; // figure6k