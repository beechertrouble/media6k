<?php
	
//
// gallery things
//


/**
* Calls the class on the post edit screen.
*/
function call_gallery6k_metaBox() {
	$gallery6k = new gallery6k_metaBox();
	$gallery6k->includes6k();
}

if ( is_admin() ) {
	add_action( 'load-post.php', 'call_gallery6k_metaBox' );
	add_action( 'load-post-new.php', 'call_gallery6k_metaBox' );
}


/**
*
*/
class gallery6k {
	
	
	public function __construct($from, $args = []) {
				
		$this->posts = [];
		
		switch(true) {
			
			case(isset($args['posts'])):
				$this->posts = $args['posts'];
				break;
			
			case(is_object($from)):
				$this->post_id = $from->ID;
				$this->posts = $this->get_media_posts(); //media6K::get_all_post_media( $this->post_id, $args);
				break;
				
			case(is_array($from)):
				$this->post_id = $from['ID'];
				$this->posts = $this->get_media_posts(); //media6K::get_all_post_media( $this->post_id, $args);
				break;
				
			case(is_int($from)):
				$this->post_id = $from;
				$this->posts = $this->get_media_posts();
				break;
			
		}
		
		
		if(!empty($this->posts)) {
			foreach($this->posts as $k => $p) {
				
				$this->posts[$k] = new media6k($p->ID);
				if(!$this->posts[$k]->has_media) unset($this->posts[$k]);
				
			}
		}
		
		return $this;
		
	} // __construct

	
	/**
	*
	*/
	public function get_media_posts() {

		$posts = [];
		$mediaIDs = get_post_meta($this->post_id, 'gallery6k_ids', true);

		if(!empty($mediaIDs)) {
			foreach($mediaIDs as $ID) {
				$posts[$ID] = get_post($ID);
			}
		}
		
		return $posts;
		
	} // get_media()
	
	
	/**
	* ... revisit this ... not sure if this is a great/sustainable idea ...
	*/
	public function get_markup($args=[]) {		
		
		$markup = '';
		
		if(!empty($this->posts)) {
			
			$markup .= '<div class="gallery6k_wrap gallery6k_' . $this->post_id . '">' . "\n";
			
			foreach($this->posts as $k => $p) {
				
				$media = new media6k($p->ID);
				$markup .= $media->img_markup('medium')  . "\n";
				
			}
			
			$markup .= '</div>' . "\n";
		}
		
		$this->markup = $markup;
		
		return $markup;
		
	} // get_markup
	
	
} // gallery6k


/**
*
*/
class gallery6k_metaBox {
	
	
	/**
	*
	*/
	public function __construct() {
		
		add_action( 'add_meta_boxes', [ $this, 'add_meta_box' ] );
		add_action( 'save_post', [$this, 'update_gallery6k' ] );
		
	} // __construct
	
	
	/**
	*
	*/
	public function add_meta_box( $post_type ) {
		
		$post_types = ['post', 'page'];     //limit meta box to certain post types
		
		if ( in_array( $post_type, $post_types )) {
			add_meta_box(
				'gallery6k_drag-n-drop',
				'Gallery 6000!', 
				[ $this, 'meta_box' ],
				$post_type,
				'advanced',
				'high'
			);
		}
				
	} // add_meta_box()
	
	
	/**
	*
	*/
	public function includes6k() {
		
		wp_enqueue_script( 'jquery-ui-draggable' );
		wp_enqueue_script( 'gallery6k_js', media6k_plugin::asset('js/gallery6k_edit_mode.js'), ['jquery-ui-draggable'], '1.0.0', true );
		wp_enqueue_style( 'gallery6k_style', media6k_plugin::asset('css/gallery6k_edit_mode.css') );
		
	} // includes6k
	
	
	/**
	* for drag and drop reordering etc..
	*/
	public function meta_box() {
		
		global $post;
		$this_ID = $post->ID;
		$nonce = wp_create_nonce('gallery6k');
		$q = new gallery6k( $post );
		// echo "\n<br/><pre>";print_r($q);echo "</pre><br />\n";exit;
		$wpurl = get_bloginfo('wpurl');
		//
		echo '<div id="gallery6k_management">
				<div class="gallery6k_section gallery6k_clearfix">
					<button class="gallery6k_getImgs button">click here to select/upload media</button>
					<ul class="">
						HINTS : 
						<li><i>&bull; save images for web at 2x intended max display width, as low quality as you can stand - try 60.</i></li>
						<li><i>&bull; shift/command-click to select multiple.</i></li>
						<li><i>&bull; drag-n-drop images below to re-order.</i></li>
					</ul>
				</div>
				<div id="gallery6k_management_content">';
		
		echo  '<div id="gallery6k_reorder_wrap" class="gallery6k_clearfix gallery6k_section">';
		
		$current_order = [];
		$galItemDetails = get_post_meta($this_ID, 'gallery6k_details', true);
		$galItemDetails = !is_array($galItemDetails) ? [] : $galItemDetails;
		
		if(!empty($q->posts)) {
						
			$cols = [
				1, 2, 3, 4
			];
			$crops = [
				'c' => 'center',
				'l' => 'left',
				'r' => 'right'
			];
			foreach($q->posts as $k => $att) {
								
				$img = new media6k($att->ID);
				$current_order[] = $att->ID;
				$src = $img->srcs->large;
				$currentCols = array_key_exists($att->ID, $galItemDetails) ? $galItemDetails[$att->ID]['cols'] : 1; 
				$currentCrop = array_key_exists($att->ID, $galItemDetails) ? $galItemDetails[$att->ID]['crop']: 1; 
				
				// echo '<img class="gallery6k_reorder" data-gallery6kid="' . $att->ID . '" src="' . $src . '" title="' . $att->post_title . '" />';
				echo '<div class="gallery6k_reorder" data-gallery6kid="' . $att->ID . '" style="background-image:url(' . $src . ');" title="' . $att->post->post_title . '">';		
					echo '<div class="gallery6k_abs-tl gallery6k-item_settings">';
						echo '<button class="gallery6k_remove button" title="remove">X</button>';
						echo '<select class="gallery6k_cols-select" name="gallery6k_cols[' . $att->ID . ']" autocomplete="off">';
						foreach($cols as $c) {
							$colSel = $c == $currentCols ? 'selected' : '';
							echo '<option value="' . $c . '" ' . $colSel . '>' . $c . ' cols</option>';
						}
						echo '</select>';
						echo '<select class="gallery6k_crop-select" name="gallery6k_crop[' . $att->ID . ']" autocomplete="off" title="crop">';
						foreach($crops as $crv => $crl) {
							$cropSel = $crv == $currentCrop ? 'selected' : '';
							echo '<option value="' . $crv . '" ' . $cropSel . '>' . $crl . '</option>';
						}
						echo '</select>';
					echo '</div>';
					echo '<div class="gallery6k_edit_mode_rel_wrap">
							<a href="' . $wpurl . '/wp-admin/post.php?post=' . $att->ID . '&action=edit" class="gallery6k_edit_link" target="_blank">
								click here to edit : 
								<em class="gallery6k_blocky gallery6k_elipsis">" ' . $att->post->post_title . ' "</em>
							</a>
						</div>';
				echo '</div>';
				
			}
			
		} else {
			
			echo '^ click the button above to select/upload media.';
			
		}
		
		echo '</div>';
		echo '<input type="hidden" id="gallery6k_order" name="gallery6k_order" value="' . implode(",", $current_order) . '" />';
		echo '<input type="hidden" name="gallery6k_noncename" value="' . $nonce . '" />';
		echo '<div><em>** remember to save/update your post when you\'re done. ; )</em></div>';
		echo '</div></div>';
				
	} // meta_box()
	
	
	/**
	* really just saving the new menu order
	*/
	public function update_gallery6k() {
		
		global $post;
		
		if(!is_object($post)) return;
		
		$this_ID = $post->ID;
		
		// verify this with nonce because save_post can be triggered at other times
		if (!isset($_POST['gallery6k_noncename']) || !wp_verify_nonce($_POST['gallery6k_noncename'], 'gallery6k')) return $this_ID;
		
		// do not save if this is an auto save routine
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $this_ID;
		
		if(isset($_POST['gallery6k_order'])) {
			$img_ids = explode(",", strip_tags(stripslashes($_POST['gallery6k_order'])));
			if(!empty($img_ids)) {
				$gal6k_ids = update_post_meta($this_ID, 'gallery6k_ids', $img_ids);
				$gal6k_details = [];
				foreach($img_ids as $i) {
					$cols = isset($_POST['gallery6k_cols']) ? $_POST['gallery6k_cols'] : [];
					$crops = isset($_POST['gallery6k_crop']) ? $_POST['gallery6k_crop'] : [];
					$gal6k_details[$i] = [
						'cols' => isset($cols[$i]) ? $cols[$i] : 1,
						'crop' => isset($crops[$i]) ? $crops[$i] : 'c'
					];
				}
				$gal6k_details = update_post_meta($this_ID, 'gallery6k_details', $gal6k_details);
			}
		}

		
		
	} // update_gallery6k()
	
	
} // gallery6k class

