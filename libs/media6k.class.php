<?php
	
/**
*
*/
class media6k {
	
	
	/**
	* $id : expecting img ID
	* if $id is is not an attachment ID, it will be assumed to be a post ID,
	* and the frist img associated with the post will be returned.
	* an original img src can be passed as well ...
	*/
	public function __construct($id) {
		
		$this->ID = (int) $id;
		$this->has_media = true;
		$this->cacheKey = 'media6k-' . $this->ID;
		$this->cached = 'narp';
		
		$cache = new beechbot_cache();
		$objects = ['post', 'srcs', 'srcs_relative'];
		
		$datas = $cache->get( $this->cacheKey);
		if ( is_null($datas) ) {
			$this->get_data();
			$datas = json_encode($this);
			$cache->set( $this->cacheKey, $datas ); // 100days
		} else {
			$datas = json_decode($datas, true);
			foreach($datas as $k => $v) {
				$this->{$k} = in_array($k, $objects) ? (object) $v : $v;
			}
			$this->cached = 'yarp';
		}
		
		return $this;
		
	} // __construct
	
	
	/** 
	* get basic data 
	*/
	private function get_data() {
				
		if(!$this->has_media) return; 	
		
		// get the first attachment if the passed id is a post ...	
		$post = property_exists($this, 'post') ? $this->post : get_post($this->ID);
		$post = !empty($post) && $post->post_type != 'attachment' ? $this->get_post_media($this->ID) : $post;
		
		if(empty($post)) {
			
			$this->has_media = false; 
			return;
			
		} else {
			
			$this->post = $post; 
			$this->ID = $this->post->ID;
			
		}
		
		$this->permalink = get_attachment_link($this->ID);
		$this->relativelink = $this->make_local_path($this->permalink);
		
		$this->original = wp_get_attachment_image_src( $this->ID, 'original', false );
		$this->original['url'] = $this->original[0];
		unset($this->original[0]);
		unset($this->original[3]);
		
		// get srcs ...
		$this->srcs = new stdClass();
		$this->srcs->original = $this->original['url'];	
		
		$this->get_alt();
		$this->get_caption();
		$this->get_my_tags();
		$this->get_my_categories();
		
		switch(true) {
			
			case(strstr($this->post->post_mime_type, 'audio')):
				$this->type = 'audio';
				// $this->get_audio_data();		
				break;
			
			case(strstr($this->post->post_mime_type, 'video')):
				$this->type = 'video';
				// $this->get_video_data();		
				break;
				
			default:
				$this->type = 'image';
				$this->get_image_data();	
				break;
			
		} 
		
	} // get_data()


	/* !Images */
	
	
	/** 
	* get img data 
	*/
	private function get_image_data() {
		
		if(is_null($this->ID)) return;
		
		$getO = $this->original;
		$this->aspect = isset($getO[2]) && isset($getO[1]) && $getO[2] > 0 && $getO[1] > 0 ? ($getO[2] / $getO[1]) * 100 : 33;
		$this->original['width'] = $getO[1];
		$this->original['height'] = $getO[2];
		unset($this->original[1]);
		unset($this->original[2]);
		
		$img6kSizes = ['large', 'medium_large', 'medium', 'thumbnail'];
		foreach($img6kSizes as $i) {
			$this->srcs->{$i} = wp_get_attachment_image_src( $this->ID, $i, false )[0];
		}
		
		$this->get_relative_srcs(); 
		
	} // get_data()
	
	
	/**
	* standard img markup ...
	* 
	* @param : string $size ( max size : default 'large' )
	* @param : array $args  
	* ['addClass'] : adds a class to the img markup
	* ['alt'] : override the default alt
	* ['attributes'] : array key => val
	*/
	public function img_markup($size='medium_large', $args=[]) {
		
		if(!$this->ID) return null;
		
		$cache = new beechbot_cache();
		$cacheKey = 'media6k-img_markup-' . json_encode(array_merge([$this->ID, $size], $args));
		$markup = $cache->get( $cacheKey ); 
		
		if(is_null($markup)) {
			
			// $src = property_exists($this->srcs_relative, $size) && !is_null($this->srcs_relative->{$size}) ? $this->srcs_relative->{$size} : $this->srcs_relative->original;
			$addClass = isset($args['addClass']) ? 'class="' . $args['addClass'] . '" ' : '';
			$alt = isset($args['alt']) ? $args['alt'] : $this->get_alt();
			$addAttr = isset($args['attributes']) ? $args['attributes'] : [];
			$attrString = '';
			
			if(!empty($addAttr)) {
				$attrString = [];
				foreach($addAttr as $k => $v) {
					$attrString[] = $k . '="' . $v . '"';
				}
				$attrString = implode(" ", $attrString);
			}
			
			$img_src = $this->make_local_path(wp_get_attachment_image_url( $this->ID, $size ));
			$img_srcset = $this->make_local_path(wp_get_attachment_image_srcset( $this->ID, $size ));
			$markup = '<img src="' . $img_src . '" srcset="' . $img_srcset . '" ' . $addClass . ' alt="' . $alt . '" ' . $attrString . ' />';
			$cache->set( $cacheKey, $markup ); 
			
		} 
		
		return $markup;	
		
	} // img_markup()
	
	/**
	* similar but for deffferd or lazy loading markup
	*/
	public function img_lazy_markup($size='medium_large', $args=[]) {
		
		$cache = new beechbot_cache();
		$cacheKey = 'media6k-img_lazy_markup-' . json_encode(array_merge([$this->ID, $size], $args));
		$markup = $cache->get( $cacheKey ); 
		
		if(is_null($markup)) {
		
			$addClass = isset($args['addWrapClass']) ? $args['addWrapClass'] : '';
			$addAspectPadding = isset($args['addAspectPadding']) ? $args['addAspectPadding'] : true;
			$aspectPadding = $addAspectPadding ? 'style="padding-top:' . $this->aspect . '%;"' : '';
			
			$markup = '<div class="media6k_lazy_markup ' . $addClass  . '" ' . $aspectPadding . '><noscript class="media6k_noscript">';
				$markup .= $this->img_markup($size, $args);
			$markup .= '</noscript></div>';
			
			$cache->set( $cacheKey, $markup ); 
			
		} 
		
		return $markup;	
		
	} // img_lazy_markup()
	
	
	/* !Video */
	
	
	/**
	*
	*/	
	public function video_markup($srcs = null, $addClass = null, $vidArgs = null) {
		
		if(!$this->ID) return null;		
		
		// defauls
		$vidSettings = [
				'src' => $this->post->guid,
				'controls' => isset($vidArgs['controls']) ? $vidArgs['controls'] : true,
				'autoplay' =>  isset($vidArgs['autoplay']) ? $vidArgs['autoplay'] : false,
				'loop' =>  isset($vidArgs['loop']) ? $vidArgs['loop'] : false,
				'muted' =>  isset($vidArgs['muted']) ? $vidArgs['muted'] : false,
				'poster' =>  isset($vidArgs['poster']) ? $vidArgs['poster'] : false,
				'preload' =>  isset($vidArgs['preload']) ? $vidArgs['preload'] : 'metadata',
				'width' =>  isset($vidArgs['width']) ? $vidArgs['width'] : false,
				'height' =>  isset($vidArgs['height']) ? $vidArgs['height'] : false
			];
		
		$vidSettingsCleaned = [];
		foreach($vidSettings as $k => $v) {
			
			if(!$v) continue;
			$vidSettingsCleaned[] = $v === true ? $k : $k . '="' . $v . '"';
			
		}	
				
		$wrapAtts = [
				'class' => 'img6k_video ' . $addClass . ' video_' . $this->ID,
				'data-id' => $this->ID,
				'data-alt' => $this->get_alt(),
				'data-permalink' => $this->permalink,
				'data-title' => $this->post->post_title,
				'data-desc' => addslashes($this->post->post_content),
				// 'data-aspect' => $this->aspect, // @todo : revisit aspect for videos ...
				//'style' => 'padding-top:' . $this->aspect . '%;'
			];
		
		$wrapAtts = array_filter($wrapAtts);
		$wrapAttsCombined = [];
		foreach($wrapAtts as $att => $val) {
			$wrapAttsCombined[] = $att . '="' . $val . '"';
		}
		
		
		$markup = '<video ' . implode(" ", $vidSettingsCleaned) . ' ' . implode(" ", $wrapAttsCombined) . '>';			
			$markup .= 'Your browser does not support the video tag.';
		$markup .= '</video>';
		
		return $markup;
		
	} // video_markup
	
	
	/* !Universal */
	
	
	/**
	*
	*/
	public function markup($size = null, $args=[]){
	
		switch(true) {
			
			case(strstr($this->post->post_mime_type, 'audio')):
				// $this->audio_markup($args);		
				break;
			
			case(strstr($this->post->post_mime_type, 'video')):
				return $this->video_markup($size, $args);
				break;
				
			default:
				return $this->img_markup($size, $args);
				break;
			
		} 	
	
	} // markup
	
	
	/**
	*
	*/
	public function make_local_path($path) {
		
		$localRoot = preg_quote(get_bloginfo('url'), '/');
		
		return  preg_replace("/" . $localRoot . "\/wp-content\/uploads\//", "/media/", $path); // substr($path, strlen($localRoot));
		
	} // make_local_path
	
	
	/**
	*
	*/
	public function get_relative_srcs() {
		
		if(!$this->has_media) return; 
		
		$this->srcs_relative = new stdClass();
		
		foreach($this->srcs as $k => $v) {
			$this->srcs_relative->{$k} = $this->make_local_path( $this->srcs->{$k} );
		}
		
		return $this->srcs_relative;
		
	} // get_relative_srcs()
		
		
	/**
	* get file path 
	*/
	public function get_file_path() {
	
		return dirname(dirname(dirname(dirname(__FILE__)))) . substr($this->srcs->original, strpos($this->srcs->original, '/wp-content'));
		
	} // get_file_path()
			

	/** 
	* get img caption
	* wp uses post_excerpt for the caption ...
	*/
	public function get_caption() {
		
		if(!$this->has_media) return; 
		
		$this->caption = $this->post->post_excerpt;
		
		return $this->caption;
		
	} // get_caption()
	
	
	/** 
	* get full caption
	* args :
	* title_wrap : default 'strong'
	* exclude : defualt null | ['title', 'post_content', 'post_excerpt']
	*/
	public function get_details($args=[]) {
		
		if(!$this->has_media) return; 
		
		$args['exclude'] = isset($args['exclude']) ? $args['exclude'] : [];
		$title_wrap = isset($args['title_wrap']) ? $args['title_wrap'] : 'strong';
		
		$details = '';
		
		if(!isset($args['exclude']['title']))
			$details .= !is_null($this->post->post_title) ? '<' . $title_wrap . '>' . $this->post->post_title . '</' . $title_wrap . '> ' : '';
			
		if(!isset($args['exclude']['post_content']))
			$details .= !is_null($this->post->post_content) && $this->post->post_content != '' ? '<p>' . preg_replace("/\n/", "<br />", $this->post->post_content) . '</p>' : '';
			
		if(!isset($args['exclude']['post_excerpt']))
			$details .= !is_null($this->post->post_excerpt) && $this->post->post_excerpt != '' ? '<p>' . preg_replace("/\n/", "<br />", $this->post->post_excerpt) . '</p>' : '';
		
		return $details == '' ? null : $details;
		
	} // get_details()
	
	
	/** 
	* get img alt text
	* wp uses _wp_attachment_image_alt for the alt ...
	*/
	private function get_alt() {
		
		if(!$this->has_media) return; 
		
		$alt = get_post_meta($this->ID, '_wp_attachment_image_alt', true);
		$alt = !is_null($alt) && !empty($alt) && $alt != '' ? $alt : $this->get_caption();
		$alt = !is_null($alt) && $alt != '' ? $alt : $this->post->post_title;
		
		$this->alt = preg_replace("/\_/", " ", addslashes($alt)); // esc_html($alt)
		
		return $this->alt; // esc_html($alt)
		
	} // get_alt()
	
	
	/**
	* get image tags
	*/
	public function get_my_tags() {
		
		if(!$this->has_media) return; 
		
		$this->tags = get_the_terms( $this->ID, 'media_tag' );
		
		return $this->tags;
		
	} // get_may_tags();
	
	
	/**
	* get image categories
	*/
	public function get_my_categories() {
		
		if(!$this->has_media) return; 
		
		$this->categories = get_the_terms( $this->ID, 'media_category' );
		
		if(!empty($this->categories)) {
			foreach($this->categories as $cid => $c) {
				
				$this->categories[$cid]->link = get_term_link($c->term_id, $c->taxonomy);
				
			}
		}
		
		return $this->categories;
		
	} // get_my_categories();
	
	
	/**
	*
	*/
	public static function get_args($args = []) {
		
		$q = [
			'post_type' => 'attachment',
			'post_status' => 'any',
			'posts_per_page' => -1,			
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'suppress_filters' => true
		];
		
		$q = array_merge($q, $args);
		
		return $q;
		
	} // get_args()
	
	
	/**
	*
	*/
	public static function get_post_media( $post_id, $args = [] ) {
	
		// no good? try the first image ...
		$q = self::get_args([
				'post_parent' => $post_id
			]);
		
		$q = array_merge($q, $args);
		
		$posts = get_posts($q);
				
		return empty($posts) ? [] : $posts[0];
		
	} // get_media()
	
	
	/**
	*
	*/
	public static function get_all_post_media( $post_id, $args = [] ) {
	
		// no good? try the first image ...
		$q = self::get_args([
				'post_parent' => $post_id
			]);
		
		$q = array_merge($q, $args);
						
		return get_posts($q);
		
	} // get_media()
	
	
	/**
	*
	*/
	public static function get_post_thumbnail_id( $post_id ) {
		
		// first try the default ...
		$id = get_post_thumbnail_id( $post_id );

		if(is_numeric($id)) return intval($id);

		// no good? try the first image ...
		$args = self::get_args([
				'post_parent' => $post_id,
				'post_mime_type' => 'image',
				'posts_per_page' => 1
			]);
			
		$id = get_posts($args);
		
		return empty($id) ? null : $id[0]->ID;
		
	} // get_post_thumbnail_id
	
	
	/**
	*
	*/
	public static function get_media_tags($args=[]) {
		
		return get_terms( 'media_tag', $args); 
		
	} // get_media_tags()


	/**
	*
	*/
	public static function get_media_categories($args=[]) {
		
		return get_terms( 'media_category', $args); 
		
	} // get_media_categories()
	
	
} // media6k class
	