<?php 
	
/**
Plugin Name: Media Jammer 6000
Description: Adds tags and categories to media, etc...
Author: beechbot
Version: 1.0
Author URI: http://beechbot.com
*/


/**
*
*/
class media6k_plugin {
	
	
	/**
	* 
	*/
	public function __construct() {
		
		$libs = __DIR__ . '/libs/';
		include($libs . 'media6k.class.php');
		include($libs . 'gallery6k.class.php');
		
		$this->libs = $libs;
		
		add_action('init', [$this, 'create_taxonomies'], 0);
		
		//add_action('admin_init', [$this, 'shortcode_helper']);
		
		add_theme_support( 'post-thumbnails' );
		
		add_action('admin_menu', [$this, 'add_media6k_bulk_edit_page']);		
		
		/*
		add_action('admin_init', [$this, 'shortcode_helper']);
		add_action('admin_menu', [$this, 'admin_pages']);
		*/
		
		// add_action('pre_get_posts', [$this, 'media6k_query']); //parse_query || pre_get_posts
		
		
	} // __construct
	
	/**
	*
	*/
	public static function asset($file) {
		
		return plugins_url($file, __FILE__);
		
	} // asset 
	
	/**
	*
	*/
	public function shortcode_helper() {
		
		
	} // shortcode_helper()
	
	
	/**
	* is_tax is not working for some reason ...
	*/
	public function media6k_query($query) {
						
		if( isset($query->query['attachment']) ) {
						
			$tag = $query->query['attachment'];
			$query->query = [ 'media_tag' => $tag ];
			$query->query_vars['attachment'] = null;
			$query->is_media_tag = true;
			$query->media_tag = $tag;
			$query->query_vars['taxonomy'] = 'media_tag';
			$query->query_vars['term'] = $query->media_tag;
			$query->is_tax = true;
			/*
			$args = [
				'post_type' => 'attachment',
				'post_status' => 'inherit',
				'tax_query' => new WP_Tax_Query([
					[
						'taxonomy' => 'media_tag',
						'field' => 'slug',
						'terms' => $tag
					]
				])
			];
			
			$query->query = [
					'media_tag' => $tag
				];
			$query->query_vars['media_tag'] = $tag;
			$query->query_vars['attachment'] = null;
			$query->query_vars['post_type'] = $args['post_type'];
			$query->post_type = $args['post_type'];
			$query->query_vars['post_status'] = $args['post_status'];
			$query->post_status = $args['post_status'];
			$query->tax_query = $args['tax_query'];
			*/			
			//$query = new WP_Query($args);
			//$wp_query->posts = $query->posts;
			//echo '<br/><pre>';print_r($query);echo '</pre><br />';exit;
			
		}
		
		return $query;
	
	} // media6k_query()
	
	
	/**
	*
	*/
	public function add_media6k_bulk_edit_page() {
		
		include($this->libs . 'media6k_bulk_edit.php');
		// wp_enqueue_script( 'jquery-ui-draggable' );
		wp_enqueue_script('jquery-ui-datepicker');
		
		// add_media_page( $page_title, $menu_title, $capability, $menu_slug, $function);
		add_media_page('Bulk Edit', 'Bulk Edit', 'read', 'media6k_bulk_edit', 'media6k_page');
		
	} // add_media6k_bulk_edit_page
	
	/**
	*
	*/
	public function create_taxonomies() {
		
		//
		// first the tags
		//
		$mediaTagName = 'Media Tag';
		$mediaTagLabels = [
			'name' => _x( $mediaTagName . 's', 'taxonomy general name' ),
			'singular_name' => _x( $mediaTagName, 'taxonomy singular name' ),
			'search_items' => __( 'Search ' . $mediaTagName . 's' ),
			'all_items' => __( 'All ' . $mediaTagName . 's' ),
			'parent_item' => __( 'Parent ' . $mediaTagName ),
			'parent_item_colon' => __( 'Parent ' . $mediaTagName . ':' ),
			'edit_item'  => __( 'Edit ' . $mediaTagName ),
			'update_item' => __( 'Update ' . $mediaTagName ),
			'add_new_item' => __( 'Add New ' . $mediaTagName ),
			'new_item_name' => __( 'New ' . $mediaTagName ),
			'menu_name' => __( $mediaTagName . 's' ),
		];
		
		// manage_terms, assign_terms, edit_terms, delete_terms
		$mediaTagCapabilities = [
			'assign_terms',
			'manage_terms',
			'edit_terms',
			'delete_terms'
		];
		
		$mediaTagArgs = [
		    'hierarchical' => false,
		    'labels' => $mediaTagLabels,
		    'public' => true,
		    'show_ui' => true,
		    'show_tagcloud' => true,
		    'show_admin_column' => true,
		    'show_in_quick_edit' => true,
		    'query_var' => true,
		    'capabilities' => $mediaTagCapabilities
		];
	
		register_taxonomy('media_tag', 'attachment', $mediaTagArgs);
		
		//
		// now the categories
		//
		$mediaCatName = 'Media Category';
		$mediaCatNamePlural = 'Media Categories';
		$mediaCatLabels = [
			'name' => _x( $mediaCatNamePlural, 'taxonomy general name' ),
			'singular_name' => _x( $mediaCatName, 'taxonomy singular name' ),
			'search_items' => __( 'Search ' . $mediaCatNamePlural ),
			'all_items' => __( 'All ' . $mediaCatNamePlural ),
			'parent_item' => __( 'Parent ' . $mediaCatName ),
			'parent_item_colon' => __( 'Parent ' . $mediaCatName . ':' ),
			'edit_item'  => __( 'Edit ' . $mediaCatName ),
			'update_item' => __( 'Update ' . $mediaCatName ),
			'add_new_item' => __( 'Add New ' . $mediaCatName ),
			'new_item_name' => __( 'New ' . $mediaCatName ),
			'menu_name' => __( $mediaCatNamePlural ),
		];
		
		// manage_terms, assign_terms, edit_terms, delete_terms
		$mediaCatCapabilities = [
			'assign_terms',
			'manage_terms',
			'edit_terms',
			'delete_terms'
		];
		
		$mediaCatArgs = [
		    'hierarchical' => true,
		    'labels' => $mediaCatLabels,
		    'public' => true,
		    'show_ui' => true,
		    'show_tagcloud' => true,
		    'show_admin_column' => true,
		    'show_in_quick_edit' => true,
		    'query_var' => true,
		    'capabilities' => $mediaCatCapabilities
		];
	
		register_taxonomy('media_category', 'attachment', $mediaCatArgs);
		
		//
		// add them to the loops	
		//
		function addMedia6kTaxToLoop( WP_Query $query ) {
	
			if ( 
				$query->is_main_query() 
				&& ( $query->is_tax('media_tag') || $query->is_tax('media_category') ) 
			) {
				
				$query->set( 'post_type', 'attachment' );
				$query->set( 'post_status', 'inherit');
				
			}
			
		} //     
		
		add_action( 'pre_get_posts', 'addMedia6kTaxToLoop');
		
	} // create_taxonomoies()
	
	
} // media6k_plugin()

new media6k_plugin();



?>