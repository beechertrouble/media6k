(function($){
	
	var media = wp.media, 
		shortcode_string = 'img6k';
		
	if(media === undefined) {
		console.log('media is undefined in img6k_edit_mode.js');
		return;
	}
		
	wp.mce = wp.mce || {};
	
	wp.mce.img6k = {
		shortcode_data: {},
		template: media.template( 'img6k_shortcode' ),
		initialize: function() {
                        var self = this;
                        self.getContent();
                },
		getContent: function() {
			var options = this.shortcode.attrs.named;
			options.innercontent = this.shortcode.content;
			return this.template(options);
		},
		View: {
			// before WP 4.2:
			template: media.template( 'img6k_shortcode' ),
			postID: $('#post_ID').val(),
			initialize: function( options ) {
				this.shortcode = options.shortcode;
				wp.mce.img6k.shortcode_data = this.shortcode;
			},
			getHtml: function() {
				var options = this.shortcode.attrs.named;
				options.innercontent = this.shortcode.content;
				return this.template(options);
			}
		},
		update: function( text, editor, node ) {
			// console.log('update', text, editor, node);
		},
		edit: function( data, update ) {
			var shortcode_data = wp.shortcode.next(shortcode_string, data);
			var values = shortcode_data.shortcode.attrs.named;
			values.innercontent = shortcode_data.shortcode.content;
			wp.mce.img6k.popupwindow(tinyMCE.activeEditor, values);
		},
		popupwindow : function(editor, values, onsubmit_callback) {
			//console.log('popup window ====> ');
			//console.log('values', values);
			values = values || [];
			if(typeof onsubmit_callback != 'function'){
				onsubmit_callback = function( e ) {
					// Insert content when the window form is submitted (this also replaces during edit, handy!)
					var s = '[' + shortcode_string;
					for(var i in e.data){
						if(e.data.hasOwnProperty(i)){
							s += ' ' + i + '="' + e.data[i] + '"';
						}
					}
					
					var classes = '';
					if(e.data.size !== undefined)
						classes += 'size-' + e.data.size;
						
					if(e.data.align !== undefined)
						classes += ' align' + e.data.align;
						
					if(classes !== '')
						s += ' classes="' + classes + '"';
					
					s += ']';
										
					editor.insertContent( s );
				};
			}
			editor.windowManager.open( {
				title: 'Img6k Edit',
				body: [
					{
						type: 'textbox',
						subtype: 'hidden',
						name: 'def_src',
						value: values['def_src']
					},
					{
						type: 'textbox',
						name: 'caption',
						label: 'Caption',
						value: values['caption']
					},
					{
						type: 'textbox',
						subtype: 'hidden',
						name: 'id',
						value: values['id']
					},
					{//add type select
						type: 'listbox',
						name: 'align',
						label: 'Alignment',
						value: values['align'],
						values: [
							{text: 'none', value: 'none'},
							{text: 'left', value: 'left'},
							{text: 'center', value: 'center'},
							{text: 'right', value: 'right'}
						]
					},
					{
						type: 'textbox',
						subtype: 'hidden',
						name: 'url',
						value: values['url']
					},
					{//add type select
						type: 'listbox',
						name: 'size',
						label: 'Size',
						value: values['size'],
						values: [
							{text: 'thumbnail', value: 'thumbnail'},
							{text: 'medium', value: 'medium'},
							{text: 'large', value: 'large'},
							{text: 'original', value: 'original'}
						]
					}
				],
				onsubmit: onsubmit_callback
			} );
		} 
	}; // wp.mce.img6k()
	wp.mce.views.register( shortcode_string, wp.mce.img6k );
	
	
	function editmodePicky() {
		
		var figs = $('iframe').contents().find('._picky').not('');
		
		figs.each(function(){
						
			if($(this).data('picked') === undefined) {
				
				$(this).data('picked', true);
				
				var id = $(this).data('id'),
					src = $(this).data('srcoriginal');
					
				$('iframe').contents().find('head').append(
					'<style type="text/css"> [data-id="' +  id + '"] { '
					+ 'background: no-repeat url(' + src + ') left top !important;'
					+ 'background-size: cover !important;'
					+ 'border: 4px dashed pink !important;'
					+ '}</style>'
				);
				
			}
			
		});
		
	}
	
	$(document).ready(function() {
		setInterval(editmodePicky, 1000);	
	});
	
			
})(jQuery);