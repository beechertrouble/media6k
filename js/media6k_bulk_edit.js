(function($){
		
	$(document).ready(function() {
		
		Array.prototype.clean = function(deleteValue) {
		  for (var i = 0; i < this.length; i++) {
		    if (this[i] == deleteValue) {         
		      this.splice(i, 1);
		      i--;
		    }
		  }
		  return this;
		};
		
				
		var $form = $("#media6k_bulk_edit"),
			$rows = $form.find('.form_row'),
			$fields = $(".form_row input, .form_row textarea, .form_row select").not('.media6k_row_select'),
			$multi_checkboxes = $form.find('.media6k_row_select'),
			$changer = $("#media6k_changed_checker"),
			$multi_luancher = $form.find('.multi_edit'),
			$modal = $(".media6k_modal"),
			$modalContent = $modal.find('.media6k_modal_content'),
			_changed = [],
			_templates = {
					tags_list : $("#media6k_tags_list_template").html(),
					tag_markup : $("#media6k_tag_markup_template").html(),
					cats_list : $("#media6k_cats_list_template").html(),
					cat_markup : $("#media6k_cat_markup_template").html(),
					other_fields : $("#media6k_other_fields_template").html(),
					multi_edit : $("#media6k_multiple_template").html()
				},
			_multiEdit = [],
			datepickerArgs = {
					dateFormat: "yy-mm-dd",
					timeFormat: "HH:mm:ss",
					prevText: "<",
					nextText: ">",
					showSecond: false,
					oneLine: true
				}
			;

		_templates.multi_edit = _templates.multi_edit.replace(/\[\_other\-fields\_\]/g, _templates.other_fields);	
			
				
		$fields
			.on('change', function(){
				
				Changed($(this).parents('.form_row'));		
				
			});
			
		$("#toggle_all_rows")
			.on('click', function(e){
				
				e.preventDefault();
				
				if($(this).text() == 'select all rows') {
					$(this).text('deselect all rows');
				} else {
					$(this).text('select all rows');
				}
				$('.media6k_row_select').click();
					
			});
			
		$multi_checkboxes
			.on('click change', function(e){
				
				var checked = $form.find('.media6k_row_select:checked'),
					count = checked.length,
					set = count > 1 ? false : true;
				
				$multi_luancher.attr({'disabled': set});
				
				_multiEdit = [];
				if(count > 0) {
					checked.each(function() {
						_multiEdit.push($(this).val());
					});
				}
				
				
			});
			
		$modal
			.on('click', function(e){
				e.preventDefault();
				e.stopPropagation();	
				$(this).addClass('media6k_hidden');
			});
			
		$modalContent
			.on('click', function(e){
				e.stopPropagation();	
			});
			
			
		$multi_luancher
			.on('click', function(e){
				e.preventDefault();
				
				// [_image-ids_]
				// [_images_]
				// _count_
				var _imageIds_ = _multiEdit.join(','),
					_images_ = '',
					count = _multiEdit.length;
				
				$(_multiEdit).each(function(idx, idee){
					_images_ += $("#media6k_thumb_" + idee)[0].outerHTML;
				});
				
				var content = _templates.multi_edit.replace(/\[\_images\_\]/g, _images_).replace(/\[\_image\-ids\_\]/g, _imageIds_).replace(/\[\_count\_\]/g, count);
				
				$modalContent.html(content);
				$modalContent.find(".media6k_datepicker").datetimepicker(datepickerArgs);
				$modalContent
					.on('click', '.media6k_toggle_tags_list', function(e){
						
						e.preventDefault();
						showTagsList($(this));
						
					})
					.on('click', '.media6k_toggle_cats_list', function(e){
						
						e.preventDefault();
						showCatsList($(this));
						
					})
					.on('click', '.media6k_add_tag, .media6k_remove_tag', function(e){
						
						e.preventDefault();
						$(this).parents('tr').find('.media6k_tags_edited_bool').val('yarp');
						tagToggle($(this));
						
					})
					.on('click', '.media6k_add_cat, .media6k_remove_cat', function(e){
						
						e.preventDefault();
						$(this).parents('tr').find('.media6k_cats_edited_bool').val('yarp');
						catToggle($(this));
						
					});
				
				$modal
					.removeClass('media6k_hidden');
					
			});
			
		$("#media6k_bulk_edit_page")
			.on('click', '.media6k_toggle_tags_list', function(e){
				
				e.preventDefault();
				showTagsList($(this));
				
			})
			.on('click', '.media6k_toggle_cats_list', function(e){
				
				e.preventDefault();
				showCatsList($(this));
				
			})
			.on('click', '.media6k_add_tag, .media6k_remove_tag', function(e){
								
				e.preventDefault();
				Changed($(this).parents('.form_row'));	
				tagToggle($(this));
				
			})
			.on('click', '.media6k_add_cat, .media6k_remove_cat', function(e){
								
				e.preventDefault();
				Changed($(this).parents('.form_row'));	
				catToggle($(this));
				
			})
			.on('click', '.media6k_inline_bulk_submit', function(e){
				
				e.preventDefault();
				
				var formData = $(this).parents('form').serializeArray(),
					formRow = $(this).parents('.form_row'),
					urlparams = window.location.search !== undefined ? window.location.search : '';
				
				formRow.addClass('saving');
								
				$.ajax({
					url : window.location.pathname + urlparams,
					method : "POST",
					data : formData,
					success : function(data, status) {
						console.log('data : ', data);
						Saved(formRow);
						formRow.removeClass('saving');
					},
					error: function(a,b,c){
						console.log(a);
						console.log(b);
						console.log(c);
						formRow.removeClass('saving');
					}	
				});
				
			});
			
		$(".media6k_modal_close")
			.on('click', function(e){
				e.preventDefault();
				$(this).parents('.media6k_modal').addClass('media6k_hidden');	
			});
			
		
		$(".media6k_datepicker").datetimepicker(datepickerArgs);	
		
		function showTagsList($el) {		
				
			if($el.hasClass('tags_added')) return;
			
			$el
				.addClass('tags_added')
				.after(_templates.tags_list);
							
		} // showTagsList()
		
		function showCatsList($el) {		
				
			if($el.hasClass('cats_added')) return;
			
			$el
				.addClass('cats_added')
				.after(_templates.cats_list);
							
		} // showCatsList()
		
		
		function Changed(row) {
			
			var ID = row.data('id');
							
			row.find('input[type="submit"]')[0].removeAttribute('disabled');
			
			if(_changed.indexOf(ID) < 0)			
				_changed.push(ID);		
				
			$changer.val(_changed.join(","));	
			
		} // chaeckChange()
		
		function Saved(row) {
			
			var ID = row.data('id');
							
			row.find('input[type="submit"]').attr('disabled', 'disabled');
			_changed = jQuery.grep(_changed, function(value) {
				return value != ID;
			});
			$changer.val(_changed.join(","));	
			
		} // chaeckChange()
		
		function tagToggle($el) {
			
			var slug = $el.data('slug').toString(),
				row = $el.parents('.inner_row'),
				tagsInput = row.find('.media6k_tag_slugs_list'),
				tagsMarkup = row.find('.current_tags_markup'),
				currentTags = tagsInput.val().split(","),
				which = $el.hasClass('media6k_add_tag') ? 'add' : 'remove';
							
			if(which == 'add') {
				if(currentTags.indexOf(slug) >= 0) return;
				currentTags.push(slug);
			} else {
				if(currentTags.indexOf(slug) < 0) return;
				currentTags.splice(currentTags.indexOf(slug), 1);
			}
			
			updateTags(currentTags, tagsInput, tagsMarkup);
			
			
		} // tagToggle()
		
		function catToggle($el) {
			
			var slug = $el.data('slug').toString(),
				row = $el.parents('.inner_row'),
				catsInput = row.find('.media6k_cat_slugs_list'),
				catsMarkup = row.find('.current_cats_markup'),
				currentCats = catsInput.val().split(","),
				which = $el.hasClass('media6k_add_cat') ? 'add' : 'remove';
											
			if(which == 'add') {
				if(currentCats.indexOf(slug) >= 0) return;
				currentCats.push(slug);
			} else {
				if(currentCats.indexOf(slug) < 0) return;
				currentCats.splice(currentCats.indexOf(slug), 1);
			}
						
			updateCats(currentCats, catsInput, catsMarkup);
			
			
		} //catToggle()
		
		function updateTags(currentTags, tagsInput, tagsMarkup) {
			
			tagsInput.val(currentTags.join(","));
				
			var newMakrup = '';
			for(var i in currentTags) {
				if(currentTags[i] !== undefined)
					newMakrup += makeTagMarkup(currentTags[i]);
			}
			
			tagsMarkup.html(newMakrup);
			
		} // updateTags()
		
		function updateCats(currentCats, catsInput, catsMarkup) {
			
			currentCats.clean("");
			catsInput.val(currentCats.join(","));
				
			var newMakrup = '';
			for(var i in currentCats) {
				if(currentCats[i] !== undefined)
					newMakrup += makeCatMarkup(currentCats[i]);
			}
			
			catsMarkup.html(newMakrup);
			
		} // updateTags()
		
		function makeTagMarkup(slug) {
			
			var temp = _templates.tag_markup,
				name = _media6k_tags_data[slug],
				markup = temp.replace(/\[slug\]/g, slug).replace(/\[name\]/g, name);

			return name !== undefined ? markup : '';
			
		} // makeTagMarkup()
		
		function makeCatMarkup(slug) {
			
			var temp = _templates.cat_markup,
				name = _media6k_cats_data[slug],
				markup = temp.replace(/\[slug\]/g, slug).replace(/\[name\]/g, name);

			return name !== undefined ? markup : '';
			
		} // makeCatMarkup()
		
	});
	
			
})(jQuery);