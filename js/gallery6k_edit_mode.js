;(function gallery6k_jammer($) {		
					
	function gallery6k_reorder() {
		
		var order = [];
		
		$("#gallery6k_reorder_wrap .gallery6k_reorder")
			.each(function(i){
				order.push($(this).data('gallery6kid'));
			});
		
		$("#gallery6k_order").val(order.join(','));
		
	} // gallery6k_reorder	
	
	function bind_gallery6k_reorder() {
		
		 $("#gallery6k_reorder_wrap")
		    	.sortable({
		    		items: ".gallery6k_reorder",
		    		placeholder: 'ui-state-highlight gallery6k_reorder',
		    		revert: true,
		    		stop: function(event, ui) {
		    			gallery6k_reorder();
		    		}
		    	});
		    	
		 $("#gallery6k_reorder_wrap .gallery6k_reorder").css({'cursor':'move'});
		
	} // bind_gallery6k_reorder
				
	$(document).ready(function(){
		
		bind_gallery6k_reorder();
		 
		// Instantiates the variable that holds the media library frame.
		var meta_image_frame;
		
		// Runs when the image button is clicked.
		$('body')
			.on('click', '.gallery6k_remove', function(e){
				
				e.preventDefault();
				
				$(this).parents('.gallery6k_reorder').remove();
				gallery6k_reorder();
				
			})
			.on('click', '.gallery6k_getImgs', function(e){
			
			// Prevents the default action from occuring.
			e.preventDefault();
			
			var meta_image = $(this),
				label = "Gallery 6000! : Select / Upload Media",
				meta_image_input = $('#gallery6k_order'),
				reOrderWrap = $("#gallery6k_reorder_wrap")
				;
			/*
			// If the frame already exists, re-open it.
			if ( meta_image_frame ) {
				wp.media.editor.open();
				return;
			}
			*/
					
			// Sets up the media library frame
			meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
				title: label,
				multiple : true,
				button: { text:  meta_image.button },
				library: { type: 'image' }
			});
			
			// Runs when an image is selected.
			meta_image_frame.on('select', function(){
			
				// return false;
							
				var media_attachments = meta_image_frame.state().get('selection').toJSON();
				
				$.each(media_attachments, function(i, o){
					
					var src = 'derp';
					
					switch(true) {
						
						case(o.sizes.large !== undefined):
							src = o.sizes.large.url;
							break;
							
						case(o.sizes.medium !== undefined):
							src = o.sizes.medium.url;
							break;
							
						case(o.sizes.full !== undefined):
							src = o.sizes.full.url;
							break;
						
					}
					
					// console.log(o);
					
					var markup = '<div class="gallery6k_reorder" data-gallery6kid="' + o.id + '" style="background-image: url(' + src + ');" title="' + o.title + '">';
						
						markup += '<div class="gallery6k_abs-tl gallery6k-item_settings">';
							markup += '<button class="gallery6k_remove button" title="remove">X</button>';
							markup += '<select class="gallery6k_cols-select" name="gallery6k_cols[' + o.id + ']" autocomplete="off">';
								markup += '<option value="1" selected="">1 cols</option>';
								markup += '<option value="2">2 cols</option>';
								markup += '<option value="3">3 cols</option>';
								markup += '<option value="4">4 cols</option>';
							markup += '</select>';
							markup += '<select class="gallery6k_crops-select" name="gallery6k_crops[' + o.id + ']" autocomplete="off">';
								markup += '<option value="c" selected="">center</option>';
								markup += '<option value="l">left</option>';
								markup += '<option value="r">right</option>';
							markup += '</select>';

						markup += '</div>';
						
						markup += '<div class="gallery6k_edit_mode_rel_wrap">';
							markup += '<a href="/wp-admin/post.php?post=' + o.id + '&action=edit" class="gallery6k_edit_link" target="_blank">click here to edit : <em class="gallery6k_blocky gallery6k_elipsis">" ' + o.title + ' "</em></a>';
						markup += '</div>';
						
					markup += '</div>';
					
					reOrderWrap.append(markup);

				});

				// meta_image_input.val(newVal.join(','));
				gallery6k_reorder();
				
			});
			
			// Opens the media library frame.
			meta_image_frame.open();
		
		});
		
	});
	
}(jQuery));